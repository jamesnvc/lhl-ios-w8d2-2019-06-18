//
//  ViewController.swift
//  StylingDemo
//
//  Created by James Cash on 18-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if self.traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass {
            print("Horizontal size class changed")
        }
        if self.traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass {
            print("vertical size class changed")
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        // need to do this for iPad, because it's wR hR in both portrait & landscape
        if view.frame.width > size.width {
            print("GOING TO PORTRAIT")
        }
    }
}

